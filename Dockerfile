FROM python:3.9-buster

WORKDIR /usr/src/app

COPY . .

EXPOSE 8080

CMD [ "python", "./main.py" ]